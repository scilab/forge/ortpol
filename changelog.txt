changelog of the Ortpol Scilab Toolbox

Ortpol (not released yet)
 * Create a demo to compute Gram matrix
 * Create a demo to use Gram-Schmidt algorithm 
   to orthogonalize with respect to a given weight.
 * Fixed bugs in the *_norm functions : 
   the squared norm was returned, instead of the norm.
 
Ortpol (v0.2 - March 2015)
 * Fixed Linux loader.
 * Added Laguerre quadrature.
 * Added Hermite quadrature.
 * Added Chebyshev quadrature.
 * Added Legendre quadrature.

Ortpol (v0.1 - February 2015)
 * Added Laguerre Polynomials.
 * Added Hermite Polynomials.
 * Added Chebyshev Polynomials.
 * Added Legendre Polynomials.

