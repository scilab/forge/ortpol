// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function demo_accuracycheby()

    // Compare the accuracy of Chebyshev nodes, 
    // 1. from the roots of the polynomial,
    // 2. from direct formula,

    // Plot when n increases

    scf();
    for n=2:50
        [x,w]=chebyshev_quadrature(n);
        p=chebyshev_poly(n);
        xp=real(roots(p));
        xp=gsort(xp,"g","i");
        if (modulo(n,2)==1) then
            x(abs(x)<1.e-7)=[];
            xp(abs(xp)<1.e-7)=[];
        end
        d=assert_computedigits(xp,x);
        dmin=min(d);
        mprintf("n=%d, dmin=%.1f\n",n,dmin)
        if (modulo(n,2)==0) then
            plot(n*ones(n,1),d,"bo")
        else
            plot((n-1)*ones(d),d,"bo")
        end
    end

    xtitle("Accuracy of Chebyshev roots");
    xlabel("n");
    ylabel("Number of accurate digits");

    ///////////////////////////////////////
    // n=50
    // Plot accuracy vs absolute value

    scf();
    n=50;
    [x,w]=chebyshev_quadrature(n);
    p=chebyshev_poly(n);
    xp=real(roots(p));
    xp=gsort(xp,"g","i");
    d=assert_computedigits(xp,x);
    dmin=min(d);
    mprintf("n=%d, dmin=%.1f\n",n,dmin)
    plot(abs(x),d,"bo")
    s=msprintf("Accuracy of Chebyshev roots (n=%d)",n)
    xtitle(s);
    xlabel("abs(x)");
    ylabel("Number of accurate digits");

    ////////////////////////////////////////
    // n=50
    // http://keisan.casio.com/exec/system/1281438499
    xexact=0.9995065603657315570007
    n=50;
    [x,w]=chebyshev_quadrature(n);
    nodecos=x($)
    //
    p=chebyshev_poly(n);
    xp=real(roots(p));
    xp=gsort(xp,"g","i");    
    noderoots=xp($)
endfunction

demo_accuracycheby()
clear demo_accuracycheby
