// Copyright (C) 2015 - 2017 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

demopath = get_absolute_file_path("ortpol.dem.gateway.sce");
subdemolist = [
"Hermite", "dem_hermite.sce"; ..
"Laguerre", "dem_laguerre.sce"; ..
"Chebyshev", "dem_chebyshev.sce"; ..
"Accuracy of Chebyshev nodes", "accuracy_chebyshev_nodes.sce"; ..
"Quadrature", "dem_quaqrature.sce"; ..
"Quadrature convergence", "dem_quadconvergence.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
