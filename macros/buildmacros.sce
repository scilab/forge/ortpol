// Copyright (C) 2010-2015 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function ortpol_macrosbuild()
    path = get_absolute_file_path("buildmacros.sce")
    tbx_build_macros("ortpol", path);
    exec(fullfile(path,"chebyshev","buildmacros.sce"));
    exec(fullfile(path,"laguerre","buildmacros.sce"));
    exec(fullfile(path,"hermite","buildmacros.sce"));
    exec(fullfile(path,"legendre","buildmacros.sce"));
endfunction

ortpol_macrosbuild();
clear ortpol_macrosbuild;
