// Copyright (C) 2013-2017 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=chebyshev_norm(n)
    // Compute Chebyshev L2 norm
    //  
    // Calling Sequence
    // y=chebyshev_norm(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a matrix of doubles, the norm
    //
    // Description
    // Compute 
    //
    // <latex>
    // \left( \int_{-1}^{1} T_n(x)^2 w(x) dx \right)^{1/2}
    // </latex>
    //
    // If n=0, 
    //
    // <latex>
    // \int_{-1}^{1} T_0(x)^2 w(x) dx = \pi
    // </latex>
    //
    // If n>0, 
    //
    // <latex>
    // \int_{-1}^{1} T_n(x)^2 w(x) dx = \frac{\pi}{2}
    // </latex>
    //
    // Examples
    // // Check int Tn(x)^2 w(x) dx
    // function y=sqTw(x,n)
    //     w=chebyshev_weight(x)
    //     P=chebyshev_eval(x,n)
    //     y=P^2*w
    // endfunction
    //
    // for n = 0:5
    //     [v,err]=intg(-1,1,list(sqTw,n));
    //     disp([n,chebyshev_norm(n),sqrt(v)])
    // end
    //    
    // Authors
    // Copyright (C) 2013 - 2017 - Michael Baudin

    apifun_checkgreq("chebyshev_norm",n,"n",1,0)
    apifun_checkflint("chebyshev_norm",n,"n",1)

    if (n==0) then
        y=sqrt(%pi)
    else
        y=sqrt(%pi/2)
    end
endfunction
