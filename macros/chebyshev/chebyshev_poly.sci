// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=chebyshev_poly(n)
    // Create Chebyshev polynomial
    //  
    // Calling Sequence
    // y=chebyshev_poly(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a polynomial
    //
    // Description
    // Create a Chebyshev polynomial of degree n at point x. 
    //
    // The Chebyshev polynomials are orthogonals with respect to 
    // the scalar product :
    //
    // <latex>
    // (f,g)=\int_{-1}^{1} f(x) g(x) w(x) dx
    // </latex>
    //
    // where w(x) is the Chebyshev weight.
    //
    // To evaluate it, we can use the horner function, 
    // but hermite_eval might be faster and more accurate.
    //
    // Examples
    //    for n=0:10
    //        y=chebyshev_poly(n);
    //        disp(y)
    //    end
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Chebyshev_polynomials
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    apifun_checkgreq("chebyshev_poly",n,"n",1,0)
    apifun_checkflint("chebyshev_poly",n,"n",1)

    // Create a Chebyshev polynomial of degree n
    if (n==0) then
        y=poly(1,"x","coeff")
    elseif (n==1) then
        y=poly([0 1],"x","coeff")
    else
        polyx=poly([0 1],"x","coeff")
        // y(n-2)
        yn2=poly(1,"x","coeff")
        // y(n-1)
        yn1=polyx
        for k=2:n
            y=2*polyx*yn1-yn2
            yn2=yn1
            yn1=y
        end
    end
endfunction
