// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function y=chebyshev_weight(x)
    // Chebyshev weight function
    //  
    // Calling Sequence
    // y=chebyshev_weight(x)
    //
    // Parameters
    // x : a matrix of doubles
    // y : a matrix of doubles, the weight
    //
    // Description
    // Computes the Chebyshev weight function:
    //
    // <latex>
    // w(x)=\frac{1}{\sqrt{1-x^2}}
    // </latex>
    //
    // for x in (-1,1).
    //
    // Examples
    // x=linspace(-0.99,0.99,1000);
    // y=chebyshev_weight(x);
    // scf();
    // plot(x,y)
    // xtitle("Chebyshev weight","x","w(x)")
    //
    // v=intg(-1,1,chebyshev_weight)
    // sqrt(%pi)
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin
    //
    apifun_checkrange ( "chebyshev_weight",x,"x",1,-1,1 )

    y=1 ./sqrt(1-x.^2)
endfunction
