// Copyright (C) 2013-2017 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=hermite_norm(n)
    // Compute Hermite L2 norm
    //  
    // Calling Sequence
    // y=hermite_norm(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a matrix of doubles, the norm
    //
    // Description
    // Compute 
    //
    // <latex>
    // \left(\int_{-\infty}^{\infty} He_n(x)^2 w(x) dx \right)^{1/2}
    // </latex>
    //
    // We have 
    //
    // <latex>
    // \left(\int_{-\infty}^{\infty} He_n(x)^2 w(x) dx \right)^{1/2}= (\sqrt{2\pi} n! )^{1/2}
    // </latex>
    //
    // Examples
    // // Check that int Hn(x)^2 w(x) dx=sqrt(2pi) n!
    // // for w(x)=exp(-x^2/2)
    // function y=sqHew(x,n)
    //     w=hermite_weight(x)
    //     P=hermite_eval(x,n)
    //     y=P^2*w
    // endfunction
    //
    // for n = 0:5
    //     [v,err]=intg(-10,10,list(sqHew,n));
    //     disp([n,hermite_norm(n),sqrt(v)])
    // end
    //    
    // Authors
    // Copyright (C) 2013 - 2017 - Michael Baudin

    apifun_checkgreq("hermite_norm",n,"n",1,0)
    apifun_checkflint("hermite_norm",n,"n",1)

    y=sqrt(sqrt(2*%pi)*factorial(n))
endfunction
