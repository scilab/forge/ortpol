// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function y=hermite_pdf(x)
    // Gaussian PDF
    //  
    // Calling Sequence
    // y=hermite_pdf(x)
    //
    // Parameters
    // x : a matrix of doubles
    // y : a matrix of doubles, the probability density
    //
    // Description
    // Computes the Gaussian probability 
    // distribution function:
    //
    // <latex>
    // f(x)=\frac{1}{\sqrt{2\pi}} \exp\left(-\frac{x^2}{2}\right)
    // </latex>
    //
    // for any real value x.
    // Its integral is equal to 1.
    //
    // Examples
    //    x=linspace(-4,4,1000);
    //    y=hermite_pdf(x);
    //    scf();
    //    plot(x,y)
    //    xtitle("Gaussian distribution","x","f(x)")
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    y=exp(-0.5*x.^2)/sqrt(2*%pi)
endfunction
