// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function [x,w]=hermite_quadrature(n)
    // Returns nodes and weights of Gauss-Hermite quadrature
    //  
    // Calling Sequence
    // [x,w]=hermite_quadrature(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0, the number of nodes.
    // x : a n-by-1 matrix of doubles, the nodes
    // w : a n-by-1 matrix of doubles, the weights
    //
    // Description
    // Returns nodes x and weights w of Gauss-Hermite quadrature.
    //
    // This implementation uses the eigenvalues of the 
    // Jacobi matrix. 
    //
    // Examples
    // [x,w]=laguerre_quadrature(6)
    //
    // Authors
    // Copyright (C) 2015 - Michael Baudin
    //
    // Bibliography
    // Numerical Mathematics, Series: Texts in Applied Mathematics, Vol. 37, Alfio Quarteroni, Riccardo Sacco, Fausto Saleri, 2nd ed. 2007

    apifun_checkgreq("hermite_quadrature",n,"n",2,0)
    apifun_checkflint("hermite_quadrature",n,"n",2)

    // Compute alphas and betas for w(x)=exp(-x^2)
    a=zeros(n,1)
    b=zeros(n,1)
    b(1)=sqrt(%pi)
    k=(2:n)'
    b(k)=0.5*(k-1)
    [x,w]=ortpol_quadrature(a,b)
    // Re-normalize weights and nodes for w(x)=exp(-0.5*x^2)
    x=x*sqrt(2)
    w=w*sqrt(2)
endfunction
