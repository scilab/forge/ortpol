// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=hermite_variance(n)
    // Variance of Hermite polynomials
    //  
    // Calling Sequence
    // y=hermite_variance(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a matrix of doubles, the variance
    //
    // Description
    // Compute the variance 
    //
    // <latex>
    // V(He_n(X))=n!
    // </latex>
    //
    // where X is a standard normal random variable.
    //
    // Examples
    // hermite_variance(0)
    // hermite_variance(1)
    // hermite_variance(2)
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin

    apifun_checkgreq("hermite_variance",n,"n",1,0)
    apifun_checkflint("hermite_variance",n,"n",1)

    if (n==0) then
        y=0
    else
        y=factorial(n)
    end
endfunction
