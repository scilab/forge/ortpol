// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function y=hermite_weight(x)
    // Gaussian weight function
    //  
    // Calling Sequence
    // y=hermite_weight(x)
    //
    // Parameters
    // x : a matrix of doubles
    // y : a matrix of doubles, the weight
    //
    // Description
    // Computes the Gaussian weight function:
    //
    // <latex>
    // w(x)=\exp\left(-\frac{x^2}{2}\right)
    // </latex>
    //
    // for any real value x.
    //
    // Examples
    //    x=linspace(-4,4,1000);
    //    y=hermite_weight(x);
    //    scf();
    //    plot(x,y)
    //    xtitle("Gaussian weight","x","w(x)")
    //
    //    v=intg(-10,10,hermite_weight)
    //    sqrt(2*%pi)
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    y=exp(-0.5*x.^2)
endfunction
