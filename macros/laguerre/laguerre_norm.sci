// Copyright (C) 2013-2017 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=laguerre_norm(n)
    // Compute Laguerre L2 norm
    //  
    // Calling Sequence
    // y=laguerre_norm(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a matrix of doubles, the norm
    //
    // Description
    // Returns the integral:
    //
    // <latex>
    // \left( \int_{0}^{\infty} L_n(x)^2 w(x) dx \right)^{1/2}
    // </latex>
    //
    // where Ln is Laguerre polynomial and w is the Laguerre weight.
    //
    // We have :
    //
    // <latex>
    // \left( \int_{0}^{\infty} L_n(x)^2 w(x) dx \right)^{1/2} = 1
    // </latex>
    //
    // Examples
    // I=laguerre_norm(0) // Norm of L0
    // I=laguerre_norm(1) // Norm of L1
    // I=laguerre_norm(2) // Norm of L2
    // 
    // // Check that int Ln(x)^2 w(x) dx=1
    // // for w(x)=exp(-x)
    // function y=sqLw(x,n)
    //     w=laguerre_weight(x)
    //     Px=laguerre_eval(x,n)
    //     y=Px^2*w
    // endfunction
    // for n = 0:5
    //     [v,err]=intg(0,100,list(sqLw,n));
    //     I=laguerre_norm(n);
    //     mprintf("n=%d, I=%f, v=%f\n",n,I,sqrt(v))
    // end
    //
    // Authors
    // Copyright (C) 2013 - 2017 - Michael Baudin

    apifun_checkgreq("laguerre_norm",n,"n",1,0)
    apifun_checkflint("laguerre_norm",n,"n",1)

    y=1
endfunction
