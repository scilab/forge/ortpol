// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function y=laguerre_pdf(x)
    // Laguerre PDF
    //  
    // Calling Sequence
    // y=laguerre_pdf(x)
    //
    // Parameters
    // x : a matrix of doubles
    // y : a matrix of doubles, the probability density
    //
    // Description
    // Computes the standard Exponential probability 
    // distribution function:
    //
    // <latex>
    // f(x)=\exp(-x)
    // </latex>
    //
    // for any real positive x.
    // Its integral is equal to 1.
    //
    // Examples
    // x=linspace(0,4,1000);
    // y=laguerre_pdf(x);
    // scf();
    // plot(x,y)
    // xtitle("Laguerre distribution","x","f(x)")
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    apifun_checkgreq("laguerre_pdf",x,"x",1,0)
    y=exp(-x)
endfunction

