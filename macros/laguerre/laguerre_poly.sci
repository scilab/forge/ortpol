// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=laguerre_poly(n)
    // Create Laguerre polynomial
    //  
    // Calling Sequence
    // y=laguerre_poly(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a polynomial
    //
    // Description
    // Create a Laguerre polynomial of degree n at point x. 
    //
    // The Laguerre polynomials are orthogonals with respect to 
    // the scalar product :
    //
    // <latex>
    // (f,g)=\int_{0}^{\infty} f(x) g(x) w(x) dx
    // </latex>
    //
    // where w(x) is the Laguerre weight.
    //
    // For n large (i.e.n>15), the evaluation of the polynomial with the 
    // horner function can be very inaccurate. 
    // To evaluate the polynomial, the laguerre_eval is 
    // more accurate.
    //
    // Examples
    // y=laguerre_poly(0) // L0
    // y=laguerre_poly(1) // L1
    // y=laguerre_poly(2) // L2
    //
    // for n=0:10
    //     y=laguerre_poly(n);
    //     disp(factorial(n)*y)
    // end
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    apifun_checkgreq("laguerre_poly",n,"n",1,0)
    apifun_checkflint("laguerre_poly",n,"n",1)

    if (n==0) then
        y=poly(1,"x","coeff")
    elseif (n==1) then
        y=poly([1 -1],"x","coeff")
    else
        polyx=poly([0 1],"x","coeff")
        // y(n-2)
        yn2=poly(1,"x","coeff")
        // y(n-1)
        yn1=1-polyx
        for k=2:n
            y=((2*k-1-polyx)*yn1-(k-1)*yn2)/k
            yn2=yn1
            yn1=y
        end
    end
endfunction
