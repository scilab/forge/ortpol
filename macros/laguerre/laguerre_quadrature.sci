// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function [x,w]=laguerre_quadrature(n)
    // Returns nodes and weights of Gauss-Laguerre quadrature
    //  
    // Calling Sequence
    // [x,w]=laguerre_quadrature(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0, the number of nodes.
    // x : a n-by-1 matrix of doubles, the nodes
    // w : a n-by-1 matrix of doubles, the weights
    //
    // Description
    // Returns nodes x and weights w of Gauss-Laguerre quadrature.
    //
    // This implementation uses the eigenvalues of the 
    // Jacobi matrix. 
    //
    // Examples
    // [x,w]=laguerre_quadrature(6)
    //
    // Authors
    // Copyright (C) 2015 - Michael Baudin
    //
    // Bibliography
    // Numerical Mathematics, Series: Texts in Applied Mathematics, Vol. 37, Alfio Quarteroni, Riccardo Sacco, Fausto Saleri, 2nd ed. 2007

    apifun_checkgreq("laguerre_quadrature",n,"n",2,0)
    apifun_checkflint("laguerre_quadrature",n,"n",2)

    // Computes alphas and betas
    k=(2:n)'
    a=zeros(n,1)
    a(1)=1.
    a(k)=1+2*(k-1)
    b=zeros(n,1)
    b(1)=1.
    b(k)=(k-1).^2
    [x,w]=ortpol_quadrature(a,b)
endfunction
