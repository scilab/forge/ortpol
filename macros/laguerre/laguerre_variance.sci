// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=laguerre_variance(n)
    // Variance of Laguerre polynomials
    //  
    // Calling Sequence
    // y=laguerre_variance(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a matrix of doubles, the variance
    //
    // Description
    // Compute the variance 
    //
    // <latex>
    // V(L_n(X))=1
    // </latex>
    //
    // for n=0,1,2,..., where X is a standard exponential random variable.
    //
    // Examples
    //    for n=0:5
    //        V=laguerre_variance(n);
    //        mprintf("n=%d, V(Ln)=%f\n",n,V)
    //    end
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    apifun_checkgreq("laguerre_variance",n,"n",1,0)
    apifun_checkflint("laguerre_variance",n,"n",1)

    if (n==0) then
        y=0
    else
        y=1
    end
endfunction
