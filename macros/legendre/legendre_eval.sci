// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=legendre_eval(x,n)
    // Evaluate Legendre polynomial
    //  
    // Calling Sequence
    // y=legendre_eval(x,n)
    //
    // Parameters
    // x : a matrix of doubles, in the interval [-1,1].
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a matrix of doubles, the function value
    //
    // Description
    // Evaluates a Legendre polynomial of degree n at point x.
    //
    // The first Legendre polynomials are
    //
    // <latex>
    // \begin{eqnarray}
    // P_0(x)&=&1, \\
    // P_1(x)&=&x
    // \end{eqnarray}
    // </latex>
    //
    // The remaining Legendre polynomials satisfy the recurrence:
    //
    // <latex>
    // \begin{eqnarray}
    // P_{n+1}(x) = \frac{2n+1}{n+1} x P_n(x) - \frac{n}{n+1} P_{n-1}(x),
    // \end{eqnarray}
    // </latex>
    //
    // for n=1,2,....
    //
    // Examples
    // x=linspace(-1,1,100);
    // y0=legendre_eval(x,0);
    // y1=legendre_eval(x,1);
    // y2=legendre_eval(x,2);
    // y3=legendre_eval(x,3);
    // h=scf();
    // plot(x,y0,"r-")
    // plot(x,y1,"g-")
    // plot(x,y2,"b-")
    // plot(x,y3,"c:")
    // xtitle("Legendre Polynomials","x","Pn(x)")
    // legend(["n=0","n=1","n=2","n=3"]);
    // h.children.data_bounds(1:2,2)=[-1.05;1.05];
    // h.children.children(4).children.thickness=3;
    // h.children.children(5).children.thickness=2;
    // h.children.children(5).children.line_style=5;
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin

    apifun_checkrange("legendre_eval",x,"x",1,-1,1)
    apifun_checkgreq("legendre_eval",n,"n",2,0)
    apifun_checkflint("legendre_eval",n,"n",2)

    if (n==0) then
        y=ones(x)
    elseif (n==1) then
        y=x
    else
        // y(n-2)
        yn2=1
        // y(n-1)
        yn1=x
        for k=2:n
            y=((2*k-1)*x.*yn1-(k-1)*yn2)/k
            yn2=yn1
            yn1=y
        end
    end
endfunction
