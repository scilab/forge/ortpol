// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function y=legendre_pdf(x)
    // Legendre PDF
    //  
    // Calling Sequence
    // y=legendre_pdf(x)
    //
    // Parameters
    // x : a matrix of doubles
    // y : a matrix of doubles, the probability density
    //
    // Description
    // Computes the Legendre probability 
    // distribution function:
    //
    // <latex>
    // f(x)=1/2
    // </latex>
    //
    // for x in [-1,1].
    // Its integral is equal to 1.
    //
    // Examples
    // x=linspace(-1,1,1000);
    // y=legendre_pdf(x);
    // h=scf();
    // plot(x,y)
    // xtitle("Legendre distribution","x","f(x)")
    // h.children.data_bounds(:,2)=[-0.1;0.6];
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    apifun_checkrange("legendre_pdf",x,"x",1,-1,1)

    y=0.5*ones(x)
endfunction
