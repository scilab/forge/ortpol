// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function [x,w]=legendre_quadrature(n)
    // Returns nodes and weights of Gauss-Legendre quadrature
    //  
    // Calling Sequence
    // [x,w]=legendre_quadrature(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0, the number of nodes.
    // x : a n-by-1 matrix of doubles, the nodes
    // w : a n-by-1 matrix of doubles, the weights
    //
    // Description
    // Returns nodes x and weights w of Gauss-Legendre quadrature.
    //
    // This implementation uses the eigenvalues of the 
    // Jacobi matrix. 
    //
    // Examples
    // [x,w]=legendre_quadrature(6)
    //
    // Authors
    // Copyright (C) 2015 - Michael Baudin
    //
    // Bibliography
    // D.P.Laurie, Computation of Gauss-type quadrature formulas, J.Comput. and Applied Maths., 127 (2001)
    // Numerical Mathematics, Series: Texts in Applied Mathematics, Vol. 37, Alfio Quarteroni, Riccardo Sacco, Fausto Saleri, 2nd ed. 2007

    apifun_checkgreq("legendre_quadrature",n,"n",2,0)
    apifun_checkflint("legendre_quadrature",n,"n",2)

    // Computes alphas and betas
    a = zeros(n,1)
    b = zeros(n,1)
    b(1)=2 // beta0
    k=(2:n)'
    b(k)=1 ./(4-(k-1).^-2)
    [x,w]=ortpol_quadrature(a,b)
endfunction
