// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function [x,w]=ortpol_quadrature(a,b)
    // Computes nodes and weights from coefficients
    //  
    // Calling Sequence
    // [x,w]=ortpol_quadrature(a,b)
    //
    // Parameters
    // a : a n-by-1 matrix of doubles, the alphas in the three term recurrence, where n is the number of nodes.
    // b : a n-by-1 matrix of doubles, the betas in the three term recurrence, where n is the number of nodes.
    // x : a n-by-1 matrix of doubles, the nodes
    // w : a n-by-1 matrix of doubles, the weights
    //
    // Description
    // We consider the three term recurrence of monic orthogonal 
    // polynomials :
    //
    // <latex>
    // \begin{eqnarray}
    // \pi_{k+1}(x) = (x-\alpha_k)\pi_{k}(x) - \beta_k \pi_{k-1}(x),
    // \end{eqnarray}
    // </latex>
    //
    // for k=0,1,...,n where 
    //
    // <latex>
    // \begin{eqnarray}
    // \alpha_k = \frac{(x\pi_k,\pi_k)}{(\pi_k,\pi_k)}
    // \end{eqnarray}
    // </latex>
    //
    // and 
    //
    // <latex>
    // \begin{eqnarray}
    // \beta_k = \frac{(\pi_k,\pi_k)}{(\pi_{k-1},\pi_{k-1})}
    // \end{eqnarray}
    // </latex>
    //
    // From these coefficients, the function computes the associated 
    // Jacobi matrix. 
    // It uses the eigenvalues and eigenvectors in order to 
    // compute the weights and nodes of the quadrature rule.
    // The nodes and weights are ordered so that the 
    // nodes x are in increasing order. 
    //
    // This is a building block to create specific quadrature 
    // rules for a given family of polynomials. 
    // For example, laguerre_quadrature uses ortpol_quadrature 
    // in order to compute the nodes and weights for 
    // Laguerre polynomials. 
    //
    // Examples
    // // For Legendre nodes:
    // n=6
    // a = zeros(n,1)
    // b = zeros(n,1)
    // b(1)=2
    // k=(2:n)'
    // b(k)=1 ./(4-(k-1).^-2)
    // [x,w]=ortpol_quadrature(a,b)
    //
    // Authors
    // Copyright (C) 2015 - Michael Baudin
    //
    // Bibliography
    // D.P.Laurie, Computation of Gauss-type quadrature formulas, J.Comput. and Applied Maths., 127 (2001)
    // Numerical Mathematics, Series: Texts in Applied Mathematics, Vol. 37, Alfio Quarteroni, Riccardo Sacco, Fausto Saleri, 2nd ed. 2007

    [lhs, rhs] = argn()
    apifun_checkrhs ( "ortpol_quadrature" , rhs , 2 )
    apifun_checklhs ( "ortpol_quadrature" , lhs , 2 )
    // Type
    apifun_checktype ( "ortpol_quadrature",a,"a",1,"constant" )
    apifun_checktype ( "ortpol_quadrature",b,"b",2,"constant" )
    // Size
    n=size(a,"*")
    apifun_checkdims ( "ortpol_quadrature",a,"a",1,[n 1] )
    apifun_checkdims ( "ortpol_quadrature",b,"b",2,[n 1] )
    //
    // Jacobi matrix
    J=diag(a)+diag(sqrt(b(2:n)),-1)+diag(sqrt(b(2:n)),1)
    // Compute eigenvectors matrix w and eigenvalues x
    [w,x]=spec(J)
    // The nodes x are just the diagonal
    x=diag(x)
    // The weights are from the first row 
    // of the eigenvector matrix
    w=b(1)*w(1,:).^2
    // Reorder the nodes and weights in increasing 
    // order of the nodes.
    [x,order]=gsort(x,"g","i")
    w=w(order)
    w=w(:)
endfunction
