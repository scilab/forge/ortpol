// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Check int Tn(x)^2 w(x) dx
function y=sqTw(x, n)
    w=chebyshev_weight(x)
    P=chebyshev_eval(x,n)
    y=P^2*w
endfunction

I=chebyshev_norm(0);
assert_checkalmostequal(I,sqrt(%pi));

for n = 1:5
    I=chebyshev_norm(n);
    assert_checkalmostequal(I,sqrt(%pi/2));
end

if (%f) then
    // Too long ...
    for n = 0:5
        abserr=1.e-5;
        [v,err]=intg(-1,1,list(sqTw,n),abserr);
        I=chebyshev_norm(n);
        d=assert_computedigits(I,sqrt(v));
        mprintf("n=%d, I=%f, v=%f, digits=%f\n",n,I,sqrt(v),d)
    end
end
