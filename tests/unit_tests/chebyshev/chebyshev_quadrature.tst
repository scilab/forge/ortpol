// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// http://keisan.casio.com/exec/system/1281438499
[x,w]=chebyshev_quadrature(6);
nodes=[
-0.965925826289068287
-0.707106781186547524
-0.258819045102520762
 0.258819045102520762
 0.707106781186547524
 0.965925826289068287
];
weights=[
0.523598775598298873
0.523598775598298873
0.523598775598298873
0.523598775598298873
0.523598775598298873
0.523598775598298873
];
assert_checkalmostequal(nodes,x,1.e-15);
assert_checkalmostequal(weights,w,1.e-15);

// Reference
// Numerical Mathematics, 
// Alfio Quarteroni, Riccardo Sacco, Fausto Saleri
// Example 10.1, p.437
// alpha=0 : f is C1
// alpha=1 : f is C2
// alpha=2 : f is C3

function y=myfunction(x,alpha)
    y=abs(x).^(alpha+3/5)
endfunction
n=30;
alpha=2;
// With Scilab
expected=intg(-1,1,list(myfunction,alpha));
[x,w]=chebyshev_quadrature(n);
y=myfunction(x,alpha);
weight=chebyshev_weight(x);
computed=sum(y.*w./weight);
assert_checkalmostequal(expected,computed,1.e-2);
