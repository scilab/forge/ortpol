// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

V=chebyshev_variance(0);
assert_checkequal(V,0);
//
for n=1:10
    V=chebyshev_variance(n);
    assert_checkequal(V,0.5);
end
//
// Check int Ln(x)^2 w(x) dx
// This is because E(Ln(X))=0, for n>0.
function y=sqLw(x, n)
    f=chebyshev_pdf(x)
    Px=chebyshev_eval(x,n)
    y=Px^2*f
endfunction

for n = 1:5
    [v,err]=intg(-1,1,list(sqLw,n),1.e-4,1.e-4);
    I=chebyshev_variance(n);
    assert_checkalmostequal(v,I,[],1.e-3);
end
