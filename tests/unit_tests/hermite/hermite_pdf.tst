// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

x=linspace(-3,3,10);
y=hermite_pdf(x);
exact=[
    0.00443184841193801  
    0.02622188909370948  
    0.09947713879274866  
    0.24197072451914337  
    0.37738322769299321  
    0.37738322769299321  
    0.24197072451914337  
    0.09947713879274865  
    0.02622188909370952  
    0.00443184841193801  
];
assert_checkalmostequal(y',exact);
//
// Check integral
v=intg(-10,10,hermite_pdf);
assert_checkalmostequal(v,1);
//
// Check consistency with respect to weight
a=-10;
b=10;
I=intg(a,b,hermite_weight);
x=linspace(a,b,10);
y1=hermite_pdf(x);
y2=hermite_weight(x);
assert_checkalmostequal(y1,y2/I);
