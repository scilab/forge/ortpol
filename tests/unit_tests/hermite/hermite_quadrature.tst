// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// http://keisan.casio.com/exec/system/1329114617
[x,w]=hermite_quadrature(6);
nodes=[
-2.350604973674492222834
-1.335849074013696949715
-0.436077411927616508679
0.436077411927616508679
1.335849074013696949715
2.350604973674492222834
];
nodes=nodes*sqrt(2);
weights=[
0.0045300099055088456409
0.1570673203228566439163
0.724629595224392524092
0.724629595224392524092
0.1570673203228566439163
0.0045300099055088456408
];
weights=weights*sqrt(2);
assert_checkalmostequal(nodes,x,1.e-15);
assert_checkalmostequal(weights,w,1.e-13);

// Reference
// http://www.wolframalpha.com/

function y=myfunction(x)
    y=1 ./(1+x.^2)
endfunction
n=20;
exact=%pi;
[x,w]=hermite_quadrature(n);
y=myfunction(x);
weight=hermite_weight(x);
computed=sum(y.*w./weight)
// TODO : fix this !!!
assert_checkalmostequal(exact,computed,1.e-0);
