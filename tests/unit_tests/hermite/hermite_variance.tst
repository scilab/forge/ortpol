// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

V=hermite_variance(0);
assert_checkequal(V,0);
//
V=hermite_variance(1);
assert_checkequal(V,1);
//
V=hermite_variance(2);
assert_checkequal(V,2);
//
V=hermite_variance(3);
assert_checkequal(V,6);
//
V=hermite_variance(4);
assert_checkequal(V,24);
//
// Check int Ln(x)^2 w(x) dx
// This is because E(Ln(X))=0, for n>0.
function y=sqLw(x, n)
    f=hermite_pdf(x)
    Px=hermite_eval(x,n)
    y=Px^2*f
endfunction

for n = 1:5
    [v,err]=intg(-7,7,list(sqLw,n),1.e-4,1.e-4);
    I=hermite_variance(n);
    assert_checkalmostequal(v,I,[],1.e-3);
end
