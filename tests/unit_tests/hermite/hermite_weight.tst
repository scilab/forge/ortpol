// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

x=linspace(-3,3,10);
y=hermite_weight(x);
exact=[
    0.01110899653824231  
    0.06572852861653045  
    0.24935220877729614  
    0.60653065971263342  
    0.94595946890676552  
    0.94595946890676552  
    0.60653065971263342  
    0.24935220877729608  
    0.06572852861653053  
    0.01110899653824231  
];
assert_checkalmostequal(y',exact);


// Check integral : must be 1.
v=intg(-10,10,hermite_weight);
exact=sqrt(2*%pi);
assert_checkalmostequal(v,exact);
