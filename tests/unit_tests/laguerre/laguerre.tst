
// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Check the accuracy of Ln(x), evaluated from the polynomial, 
// or from the recurrence.


// With one single x, n
// Reference
// http://www.wolframalpha.com/input/?i=laguerre%28100%2C10%29
n=100;
x=10;
L=laguerre_poly(n);
y1=horner(L,x); // Not accurate
y2=laguerre_eval(x,n); // Accurate
exact=13.277662844303454137789892644070318985922997400185621;
assert_checkalmostequal(y1,exact,1); // Not accurate
assert_checkalmostequal(y2,exact,10*%eps); // Accurate
//
// With various x, n
R=[];
nmax=100;
xmax=21;
i=0;
j=0;
for x=1:4:xmax
    i=i+1;
    j=0;
    for n=1:15
        j=j+1;
        L=laguerre_poly(n);
        y1=horner(L,x);
        y2=laguerre_eval(x,n);
        d=assert_computedigits(y1,y2,2);
        d=floor(d);
        assert_checktrue(d>20);
    end
end
