// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

x=linspace(0,4,10);
y=laguerre_weight(x);
exact=[
    1.                   
    0.64118038842995451  
    0.41111229050718751  
    0.26359713811572677  
    0.16901331540606609  
    0.10836802322189587  
    0.06948345122280154  
    0.04455142624448969  
    0.02856550078455038  
    0.01831563888873418  
];
assert_checkalmostequal(y',exact);


// Check integral : must be 1.
v=intg(0,30,laguerre_weight);
assert_checkalmostequal(v,1);
