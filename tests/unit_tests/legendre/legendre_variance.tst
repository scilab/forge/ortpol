// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

V=legendre_variance(0);
assert_checkequal(V,0);
//
V=legendre_variance(1);
assert_checkalmostequal(V,1/3);
//
V=legendre_variance(2);
assert_checkalmostequal(V,0.2);
//
V=legendre_variance(3);
assert_checkalmostequal(V,1/7);
//
// Check int Ln(x)^2 w(x) dx
function y=sqLw(x, n)
    f=legendre_pdf(x)
    Px=legendre_eval(x,n)
    y=Px^2*f
endfunction

for n = 1:5
    [v,err]=intg(-1,1,list(sqLw,n),1.e-4,1.e-4);
    I=legendre_variance(n);
    assert_checkalmostequal(v,I,[],1.e-3);
end
